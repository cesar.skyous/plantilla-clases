import { Link } from "react-router-dom";
import BarraInicial from "../../components/examplefacecomponents/BarraInicial";
import Card1 from "../../components/examplefacecomponents/cards/Card1";
import Navigation from "../../components/nav/Navigation";

export default function App() {
    const callApi = () => {
        //consume api
        const tarjetas = [
            {
                title: 'Titulo 1',
                style: 'rid bg-student'
            },
            {
                title: 'Titulo 2',
                style: 'rid bg-student'
            },
            {
                title: 'Titulo 3',
                style: 'rid bg-student'
            },
            {
                title: 'Titulo 4',
                style: 'rid bg-student'
            },
        ];
        const data = tarjetas.map((tarjeta) => {
            return <Card1 title={tarjeta.title} styleType={tarjeta.style} />
        });

        return data;
    }

    return (
    <>
        <BarraInicial/>
        <Navigation opciones={[{route:'/academico', ref:'Academico'},{route:'/academico', ref:'Academico'},{route:'/academico', ref:'Academico'},{route:'/academico', ref:'Academico'},{route:'/academico', ref:'Academico'}]}/>
        <div className="grid content-center">
            {/* <a href="#" className="text-center">Link Centrado</a> */}
            <Link to={'/contacto'}>Ir a contacto</Link>
        </div>
        <div className="grid grid-cols-4 grid-gap-2 content-center mx-auto w-90">
            {/* <Card1 title="Titulo" styleType="grid bg-student"/>
            <Card1 title="Titulo" styleType="grid bg-student"/>
            <Card1 title="Titulo" styleType="grid bg-student"/>
            <Card1 title="Titulo" styleType="grid bg-student"/> */}
            {callApi()}
        </div>
    </>
    );
}
