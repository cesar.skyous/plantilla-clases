import {Home, Contacto, Blog, Producto} from "./pages";

const routes = [
    {path: "/",element: <Home/>},
    {path: "/contacto",element: <Contacto/>},
    {path: "/blog",element: <Blog/>},
    {path: "/product",element: <Producto/>},
];

export default routes;
