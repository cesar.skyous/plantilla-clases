import React from 'react';
import ReactDOM from 'react-dom/client';
import './assets/styles/styles.css';
import {
    RouterProvider,
  } from "react-router-dom";
import router from './routes/Router';
//TODO Implementar el navegador en todas las paginas y poner datos en duro simulando la informacion
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render (<React.StrictMode>
    <RouterProvider router={router} />
</React.StrictMode>);
